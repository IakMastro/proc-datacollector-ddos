#!/bin/bash

echo "=====> Launching docker instances (this step may take a while depending on your internet speed"

docker-compose up -d

echo "====> Running script"

docker exec -it ddos ./ddos

echo "===> Close machines"

docker-compose down

echo "===> Done"
echo "Try to fix this"
